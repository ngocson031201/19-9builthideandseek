﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    [SerializeField] Text CoinTextMenu;
    [SerializeField] Text CoinTextInsideGame;
    [SerializeField] Text CoinTextInShopBuy;
    [SerializeField] Text CoinTextInShopIAP;
    [SerializeField] Text CoinTextInShopProfile;
    [SerializeField] Text CoinTextSetting;
    [SerializeField] Text Yourscore;
    [SerializeField] Text YourscoreLose;
    [SerializeField] Text Arrested;
    public int Coins;
    public int coin = 0;
    public int coinVisibleGUI;
    private void Awake()
    {
        Coins = Prefs.COIN;//lưu lại coin mà khi chơi game thì player kiếm được 
        CoinTextInShopBuy.text = Coins.ToString();
        CoinTextInShopIAP.text = Coins.ToString();
        CoinTextInShopProfile.text = Coins.ToString();
        CoinTextSetting.text = Coins.ToString();
        coinVisibleGUI = 0;
        instance = this;
    }

    private void Start()
    {
        coin = Prefs.COIN;
        CoinTextMenu.text = coin.ToString();
        CoinTextInsideGame.text = coin.ToString();
        CoinTextSetting.text = coin.ToString();


        int random = Random.Range(50, 100);
        Yourscore.text = random.ToString();
        YourscoreLose.text = random.ToString();
    }

    private void Update()
    {

        Arrested.text = PatrolAgent.HideArrested.ToString();
    }
    public void AddPoint()
    {
        coin++;
        coinVisibleGUI++;
        Prefs.COIN = coin;
        CoinTextMenu.text = Prefs.COIN.ToString();
        CoinTextInsideGame.text = Prefs.COIN.ToString();
    }

    public void AddPointGold()//1gold =10coin
    {
        coin = coin + 10;
        coinVisibleGUI = coinVisibleGUI + 10;
        Prefs.COIN = coin;
        CoinTextMenu.text = Prefs.COIN.ToString();
        CoinTextInsideGame.text = Prefs.COIN.ToString();
    }

    public void AddPointDiamond()//1diamond =20coin
    {
        coin = coin + 20;
        coinVisibleGUI = coinVisibleGUI + 20;
        Prefs.COIN = coin;
        CoinTextMenu.text = Prefs.COIN.ToString();
        CoinTextInsideGame.text = Prefs.COIN.ToString();
    }

    public void AddCoinWithColisionHide(int index)
    {
        coin = coin + index;
        Prefs.COIN = coin;
        CoinTextMenu.text = Prefs.COIN.ToString();
        CoinTextInsideGame.text = Prefs.COIN.ToString();
    }

    public void AddCoinDaily(int index)
    {
        coin = coin + index;
        Prefs.COIN = coin;
        CoinTextMenu.text = Prefs.COIN.ToString();
        CoinTextInsideGame.text = Prefs.COIN.ToString();
        CoinTextInShopBuy.text = Prefs.COIN.ToString();
        CoinTextInShopIAP.text = Prefs.COIN.ToString();
        CoinTextInShopProfile.text = Prefs.COIN.ToString();
    }    

    //những hàm dưới là hàm viết cho shop cửa hàng
    public void UseCoins(int amount)//khi mua thì sẽ thực hiện sự kiện này
    {
        Coins -= amount;
        CoinTextInShopBuy.text = Coins.ToString();
        CoinTextMenu.text = Coins.ToString();
        CoinTextInsideGame.text = Coins.ToString();
        CoinTextInShopProfile.text = Coins.ToString();
        Prefs.COIN = Coins;
    }   
    
    public bool HasEnoughCoins(int amount)//có đủ tiền
    {
        return (Prefs.COIN >= amount);
    }    

}
