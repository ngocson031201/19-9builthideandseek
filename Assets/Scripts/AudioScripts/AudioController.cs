using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AudioController : MonoBehaviour
{
    #region parameter
    public static AudioController instance;
    AudioSource AudioControllerSource;
    public AudioClip[] BGM;
    public AudioClip[] CoinMusics;
    public AudioClip WinMusic;
    public AudioClip LoseMusic;
    public AudioClip ClickMusic;
    public AudioClip BulletMusic;
    #endregion
    public void Awake()
    {
        instance = this;
        AudioControllerSource = GetComponent<AudioSource>();
        AudioControllerSource.clip = BGM[Random.Range(0, BGM.Length)];
        AudioControllerSource.Play();
    }

    public void CoinMusic()
    {
        if(UIController.instance.isPlayGame==true)
        {
            AudioControllerSource.clip = CoinMusics[Random.Range(0, CoinMusics.Length)];
            AudioControllerSource.PlayOneShot(AudioControllerSource.clip);
        }
    }

    public void WinSound()
    {
        AudioControllerSource.PlayOneShot(WinMusic);
    }

    public void LoseSound()
    {
        AudioControllerSource.PlayOneShot(LoseMusic);
    }

    public void BulletSound()
    {
        AudioControllerSource.PlayOneShot(BulletMusic);
    }

    public void ClickSound()
    {
        AudioControllerSource.PlayOneShot(ClickMusic);
    }
    public void StopBGM()
    {
        AudioControllerSource.Stop();
    }

}
