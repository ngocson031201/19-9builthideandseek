using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioInsideGame : MonoBehaviour
{
    public static AudioInsideGame instance;
    AudioSource AudioControllerSource;
    public AudioClip[] BGM;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        AudioControllerSource = GetComponent<AudioSource>();

    }

    public void SoundIngame()
    {
        AudioControllerSource.clip = BGM[Random.Range(0, BGM.Length)];
        AudioControllerSource.Play();
    }

    public  void StopSoundInside()
    {
        AudioControllerSource.Stop();
    }

}
