using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SoundControl : MonoBehaviour
{
    [SerializeField] Slider VolumeSlider;

    private void Start()
    {
        if(!PlayerPrefs.HasKey("MusicVolume"))
        {
            PlayerPrefs.SetFloat("MusicVolume", 1);
            Load();
        }
        else
        {
            Load();
        }    
    }

    public void ChangeVolume()
    {
        AudioListener.volume = VolumeSlider.value;
        Save();
    }


    public void Load()
    {
        VolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume");

    }    

    private void Save()
    {
        PlayerPrefs.SetFloat("MusicVolume", VolumeSlider.value);
    }    

}
