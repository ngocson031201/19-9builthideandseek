using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSkinAI : MonoBehaviour
{
    public  GameObject[] skins;
    void Start()
    {
        changeSkins();
    }


    private void changeSkins()
    {
        int selectedIndex = Random.Range(0, skins.Length);
        skins[selectedIndex].SetActive(true);
        for (int i = 0; i < skins.Length; i++)
        {
            if (i != selectedIndex)
            {
                skins[i].SetActive(false);
            }
        }
    }
}
