﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Prefs
{
    public static int COIN
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.COIN_N, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.COIN_N);
    }
    public static float PERCENT
    {
        set
        {
            PlayerPrefs.SetFloat(PreConsts.HEALTHBAR_R, value);
        }
        get => PlayerPrefs.GetFloat(PreConsts.HEALTHBAR_R);
    }

    public static float VOLUME
    {
        set
        {
            PlayerPrefs.SetFloat(PreConsts.VOLUME_E, value);
        }
        get => PlayerPrefs.GetFloat(PreConsts.VOLUME_E);
    }

    public static int LEVEL
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.LEVEL_L, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.LEVEL_L);
    }

    public static int KEY
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.KEY_Y, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.KEY_Y);
    }


    public static int SKINID
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.SKIN_N, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.SKIN_N);
    }


    public static int DAY
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.DAY_Y, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.DAY_Y);
    }

    public static int CHECKUNLOCKALLSKIN
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.CHECKUNLOCKALLSKIN_N, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.CHECKUNLOCKALLSKIN_N);
    }

    public static int BANNERADS
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.BANNERADS_R, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.BANNERADS_R);
    }

    public static int FULLADS
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.FULLADS_S, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.FULLADS_S);
    }

    public static int REWARDADS
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.REWARDADS_S, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.REWARDADS_S);
    }
    public static int CHECKSTARTERPACK
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.CHECKSTARTERPACK_K, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.CHECKSTARTERPACK_K);
    }

    public static int SKINLOADINPROGRESS
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.SKININPROGRESS_S, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.SKININPROGRESS_S);
    }

    public static int AVATARDEFAULT
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.AVATAR_R, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.AVATAR_R);
    }
    public static int DAILY_FIRST
    {
        set
        {
            PlayerPrefs.SetInt(PreConsts.DAILY_FIRST, value);
        }
        get => PlayerPrefs.GetInt(PreConsts.DAILY_FIRST);
    }
    public static string DAILY_CLICK
    {
        set
        {
            PlayerPrefs.SetString(PreConsts.DAILY_CLICK, value);
        }
        get => PlayerPrefs.GetString(PreConsts.DAILY_CLICK);
    }
    public static string TIMETODAY
    {
        set
        {
            PlayerPrefs.SetString(PreConsts.TIME_TODAY, value);
        }
        get => PlayerPrefs.GetString(PreConsts.TIME_TODAY);
    }
}
